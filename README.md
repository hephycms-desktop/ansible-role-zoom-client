Role Name
=========

Install ZOOM client

http://zoom.us

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None.

Example Playbook
----------------

    - hosts: desktop
      roles:
         - dietrichliko.zoom

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
